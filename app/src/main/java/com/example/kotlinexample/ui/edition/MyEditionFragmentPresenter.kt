package com.example.kotlinexample.ui.edition

class MyEditionFragmentPresenter : MyEditionFragmentContract.Presenter {

    private var view: MyEditionFragmentContract.View? = null


   override fun OnStartView(view: Any) {


    }

    override fun OnStopView() {
        view=null
    }

    override fun eventClickAdd() {
        view!!.onAddCLick()
    }

    override  fun eventClickRemove() {
        view!!.onRemoveClick()

    }
}