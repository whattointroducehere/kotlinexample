package com.example.kotlinexample.ui.edition

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.example.kotlinexample.R
import com.example.kotlinexample.model.MyBook
import io.realm.Realm
import io.realm.RealmConfiguration

class MyEditionFragment : Fragment(), MyEditionFragmentContract.View {

    private val presenter: MyEditionFragmentContract.Presenter? = null

    internal var mEditTitle: EditText? = null

    private var mRealm: Realm? = null

    private val trimmedTitle: String
        get() = mEditTitle!!.text.toString().trim { it <= ' ' }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val config = RealmConfiguration.Builder()
            .build()
        mRealm = Realm.getInstance(config)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_edition, container, false)
    }


    override fun onDestroy() {
        super.onDestroy()
        mRealm!!.close()
    }

    override fun onAddCLick() {
        mRealm!!.beginTransaction()
        val book = mRealm!!.createObject(MyBook::class.java!!)
        book.setTitle(trimmedTitle)
        mRealm!!.commitTransaction()
    }

    override fun onRemoveClick() {
        mRealm!!.beginTransaction()
        val books = mRealm!!.where(MyBook::class.java!!).equalTo("title", trimmedTitle).findAll()
        if (!books.isEmpty()) {
            for (i in books.indices.reversed()) {
                books.get(i)!!.deleteFromRealm()

            }
        }
        mRealm!!.commitTransaction()
    }
}