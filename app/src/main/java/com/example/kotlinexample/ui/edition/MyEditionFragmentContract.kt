package com.example.kotlinexample.ui.edition

import com.example.kotlinexample.presentation.base.BasePresenter

interface MyEditionFragmentContract {

    interface View {

        fun onAddCLick()

        fun onRemoveClick()

    }

    interface Presenter : BasePresenter<Any> {

        fun eventClickAdd()

        fun eventClickRemove()
    }
}