package com.example.kotlinexample.model

import io.realm.RealmObject
import io.realm.annotations.Required

   open class MyBook : RealmObject() {
    fun getTitle(): String? {
        return title
    }

    fun setTitle(title: String) {
        this.title = title
    }

    @Required
    private var title: String? = null


}