package com.example.kotlinexample.presentation.base

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v7.app.AppCompatActivity
import android.widget.Toast

abstract class BaseActivity <Binding : ViewDataBinding> : AppCompatActivity() {
    private lateinit var binding: ViewDataBinding

    protected var presenter : BasePresenter<Any>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView<Binding>(this, getLayoutRes())
        initView()
    }
    @LayoutRes
    protected abstract fun getLayoutRes(): Int

    protected fun getBinding(): ViewDataBinding {
        return binding
    }

    protected abstract fun initView()

    public override fun onDestroy() {
        super.onDestroy()
        }


    protected fun toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    protected abstract fun onDestroyView()


}