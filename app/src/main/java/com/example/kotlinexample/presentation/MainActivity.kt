package com.example.kotlinexample.presentation

import android.databinding.DataBindingUtil
import android.os.Bundle
import com.example.kotlinexample.R
import com.example.kotlinexample.databinding.ActivityMainBinding
import com.example.kotlinexample.presentation.base.BaseActivity
import com.example.kotlinexample.presentation.base.BasePresenter
import com.example.kotlinexample.ui.edition.MyEditionFragment
import com.example.kotlinexample.ui.list.MyListFragment


class MainActivity : BaseActivity<ActivityMainBinding>(), IPresenter.View {



    override fun getLayoutRes(): Int = R.layout.activity_main

        override  fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_main)
            if (savedInstanceState == null) {
                supportFragmentManager
                    .beginTransaction()
                    .add(R.id.edition_container, MyEditionFragment())
                    .add(R.id.list_container, MyListFragment())
                    .commit()
            }

        }


    override  fun initView() {
        presenter = Presenter()
        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this,  getLayoutRes())
        presenter!!.init()
    }

    override fun onDestroyView() {

    }



}