package com.example.kotlinexample.presentation

class Presenter : IPresenter.EventListener {
    private var view: IPresenter.View? = null
    override fun init() {

    }

    override fun OnStartView(view: IPresenter.View) {
        this.view=view
    }

    override fun OnStopView() {
        if (view!=null) view = null
    }









}