package com.example.kotlinexample.presentation.base


interface BasePresenter<V> {
    fun OnStartView(view: V)

    fun OnStopView()
}