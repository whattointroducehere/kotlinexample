package com.example.kotlinexample.presentation

import com.example.kotlinexample.presentation.base.BasePresenter

interface IPresenter {

    interface View

    interface EventListener : BasePresenter<View>{
        fun init()
    }

}