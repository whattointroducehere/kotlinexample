package com.example.kotlinexample.app

import android.app.Application
import com.example.kotlinexample.BuildConfig
import timber.log.Timber

class App : Application() {

    open override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}